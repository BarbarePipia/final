package com.example.todolist

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        getSupportActionBar()?.hide()
        init()
    }

    private fun init(){
        auth = Firebase.auth
        signupButton.setOnClickListener {
            signUp()


        }
    }

    private fun signUp(){
        val email:String = signupEmail.text.toString()
        val password:String = signupPassword.text.toString()
        val repeatPassword:String = repeatPassword.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if (password==repeatPassword){
                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    progressBarsignup.visibility = View.VISIBLE
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            progressBarsignup.visibility = View.GONE
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("signUp", "createUserWithEmail:success")
                                val user = auth.currentUser

                                val intent = Intent(this, Signupsucc::class.java)
                                startActivity(intent)

                            } else {
                                // If sign in fails, display a message to the user.
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                            }
                        }
                    Toast.makeText(this,"SignUp is Success!",Toast.LENGTH_SHORT).show()
                }else{Toast.makeText(this,"Email format is not Correct!",Toast.LENGTH_SHORT).show()}

            }else{Toast.makeText(this,"Please type same Passwords!",Toast.LENGTH_SHORT).show()}
        } else{
            Toast.makeText(this,"Please fill all fields!",Toast.LENGTH_SHORT).show()
        }
    }





}