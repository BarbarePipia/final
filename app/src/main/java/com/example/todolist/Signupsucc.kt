package com.example.todolist

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_signupsucc.*

class Signupsucc : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signupsucc)
        getSupportActionBar()?.hide()
        backToMain()
    }
    private fun backToMain(){
        back.setOnClickListener {
            val intent = Intent(this, authenticationActivity::class.java)
            startActivity(intent)
        }
    }
}